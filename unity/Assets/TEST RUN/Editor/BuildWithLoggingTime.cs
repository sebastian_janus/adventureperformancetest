﻿using UnityEditor;
using UnityEngine;

public class BuildWithLoggingTime
{
    [MenuItem( "PerformanceTest/Measure building times (android)" )]
    public static void BuildAndroid()
    {
        BuildAndroid( null );
    }

    public static void BuildAndroid( string path )
    {
        // Get filename.
        if( path == null )
        {
            path = EditorUtility.SaveFolderPanel( "Choose Location of Built Game", "", "" );
        }
        string[] levels = new string[] { "Assets/Scenes/Persistent.unity", "Assets/Scenes/SecurityRoom.unity", "Assets/Scenes/Market.unity" };

        // Build player.        

        // warmup
        EditorUserBuildSettings.SwitchActiveBuildTarget( BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64 );

        // android switch platform
        double androidSwitchStart = EditorApplication.timeSinceStartup;
        EditorUserBuildSettings.SwitchActiveBuildTarget( BuildTargetGroup.Android, BuildTarget.Android );
        double androidSwitchEnd = EditorApplication.timeSinceStartup;

        // print results
        Debug.Log( "Android switch platform time: " + ( androidSwitchEnd - androidSwitchStart ) );

        // android mono build
        PlayerSettings.SetScriptingBackend( BuildTargetGroup.Android, ScriptingImplementation.Mono2x );
        double androidMonoStart = EditorApplication.timeSinceStartup;
        BuildPipeline.BuildPlayer( levels, path + "/BuiltGameMono.apk", BuildTarget.Android, BuildOptions.None );
        double androidMonoEnd = EditorApplication.timeSinceStartup;

        // print results
        Debug.Log( "Android build time (mono): " + ( androidMonoEnd - androidMonoStart ) );

        // android il2cpp build
        PlayerSettings.SetScriptingBackend( BuildTargetGroup.Android, ScriptingImplementation.IL2CPP );
        double androidCppStart = EditorApplication.timeSinceStartup;
        BuildPipeline.BuildPlayer( levels, path + "/BuiltGameIl2cpp.apk", BuildTarget.Android, BuildOptions.None );
        double androidCppEnd = EditorApplication.timeSinceStartup;

        // print results
        Debug.Log( "Android build time (il2cpp): " + ( androidCppEnd - androidCppStart ) );
    }

    [MenuItem( "PerformanceTest/Measure building times (standalone windows)" )]
    public static void BuildWindows()
    {
        BuildWindows( null );
    }

    public static void BuildWindows( string path )
    {
        // Get filename.
        if( path == null )
        {
            path = EditorUtility.SaveFolderPanel( "Choose Location of Built Game", "", "" );
        }
        string[] levels = new string[] { "Assets/Scenes/Persistent.unity", "Assets/Scenes/SecurityRoom.unity", "Assets/Scenes/Market.unity" };

        // windows 64 switch
        double windowsSwitchStart = EditorApplication.timeSinceStartup;
        EditorUserBuildSettings.SwitchActiveBuildTarget( BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64 );
        double windowsSwitchEnd = EditorApplication.timeSinceStartup;

        // print results   
        Debug.Log( "Windows64 switch platform time: " + ( windowsSwitchEnd - windowsSwitchStart ) );

        // windows64 build
        double windowsStart = EditorApplication.timeSinceStartup;
        BuildPipeline.BuildPlayer( levels, path + "/BuiltGame64.apk", BuildTarget.StandaloneWindows64, BuildOptions.None );
        double windowsEnd = EditorApplication.timeSinceStartup;

        // print results   
        Debug.Log( "Windows64 build time: " + ( windowsEnd - windowsStart ) );
    }

    [MenuItem( "PerformanceTest/Measure building times (all platforms)" )]
    public static void BuildAll()
    {
        string path = EditorUtility.SaveFolderPanel( "Choose Location of Built Game", "", "" );
        BuildAndroid( path );
        BuildWindows( path );
    }
}
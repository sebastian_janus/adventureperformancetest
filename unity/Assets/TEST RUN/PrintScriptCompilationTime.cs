﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class PrintScriptCompilationTime : MonoBehaviour
{        
    bool wasCompiling = false;
    double compilationStartTime = 0;

    private void Update()
    {
        if( !wasCompiling && EditorApplication.isCompiling )
        {
            compilationStartTime = EditorApplication.timeSinceStartup;
            wasCompiling = true;
        }
        else if( wasCompiling && !EditorApplication.isCompiling )
        {
            wasCompiling = false;
            Debug.Log( "Scripts compilation time: " + ( EditorApplication.timeSinceStartup - compilationStartTime ) );
        }
    }
}
#endif

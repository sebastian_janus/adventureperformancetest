# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains source code for measurements that I made for comparing CPU performance for Unity3d work.

It consists of test measuring compressing assets, whole platform switching (mainly assets reimport), building mobile (android mono/android il2cpp) and standalone (windows64) platforms, and script compilation time in small project.
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Download Unity 2017.1.1f1. (standalone and android modules)

Download Android SDK version 26.
Download JDK 8u144 (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
Download NDK r10e (https://developer.android.com/ndk/downloads/older_releases.html)

Set proper paths to these SDK in Unity preferences.

1. For testing switch platform and build times, select `Performance Test/Measure building times (all platforms)`. Time results are printed onto console.
2. For testing GI baking time, open Market scene, open window `Lighting` and press `Generate lightmap`. Then on windows go to `UserDirectory\AppData\Local\Unity\` and open `Editor.log` file. There you should find overal lightmap generating time in minutes and baking lightmap time in seconds.
3. For testings one texture specific compression time, switch to proper platform, select file `FruitVendorAlbedo.png`, select `Override settings` for current platform, then choose `FruitVendorAlbedo RGB Crunched DXT1 50` on windows or `FruitVendorAlbedo PVRTC 2bit best` and `FruitVendorAlbedo PVRTC 2bit normal` on android platform. You can find compression time in `Editor.log` file again.
4. For testing script compilation time, open scene `ScriptCompilationTimeTestScene` and open `ScriptToChange.cs`. Make some changes in this source file, uncomment section, whatever. Then save and switch back to Unity editor - compilation time should be printed on the console.
